//  Total Number Of Data Points = 300
//  Smallest value is 5238
//  Largest value = 12666
// DAC Data Transfer

#define SLAVESELECT 10 // CS
#define DATAOUT 11 // DIN
#define SPICLOCK 13 // SCLK

int dataset[]={
  9618, 9596, 9593,10164,9706,9917,9904,10344,10229,10326,10223,9851,10151,9895,9813,9772,9612,9197,9292,9331,9384,9352,9513,9301,9374,8907,8615,8340,8439,8404,8682,8736,8642,8701,8915,8913,8720,8821,8810,8110,8184,8020,8197,7817,8055,8145,7758,7358,7354,7344,6965,7200,7195,7246,7388,7316,7254,7202,7204,7255,7317,7501,7132,7187,7150,7277,6623,6891,7118,7064,7258,7198,7210,7224,7338,7510,7488,7527,7389,7194,7292,7294,7390,7541,7739,7419,7508,7156,7109,6903,7311,7268,7627,8098,8133,8187,8502,8491,8471,8646,8777,8821,8733,9039,8809,8773,9216,9339,9308,9393,9194,9289,9231,9140,9154,9407,9205,9529,9230,8655,7412,7466,8020,8197,8223,7948,7947,8070,7991,8159,8348,8275,8300,8258,8558,8560,8168,8190,7854,8126,8137,8223,8359,8292,8289,8043,8198,8177,8063,8430,8694,9702,10028,9986,10168,10244,10156,10187,10251,10302,10335,10342,10415,10158,10098,10303,10381,10455,10298,10564,10567,10615,10340,9724,9587,9584,9498,9721,10185,10372,10142,10134,10389,10099,10142,10760,10915,10306,10216,10331,10293,10050,10892,11396,11549,11309,11879,11999,11975,11481,11828,10929,10816,10523,10374,10080,9574,9507,9541,9476,9824,9879,9763,9864,10340,10589,10740,10504,10627,9667,9439,10838,10174,11355,11757,11342,12108,12543,12238,11406,11256,10970,11145,11940,10842,10624,10854,11903,12400,11329,12666,11820,11056,10906,10729,10159,9517,9255,9081,9313,8953,8808,8650,8218,8127,7884,7982,7628,7893,7980,7786,7776,7687,8115,8725,8544,8555,8269,8645,8715,8756,8614,8025,7964,7851,7628,7904,7938,8148,7257,7355,7866,8169,7947,7790,6967,7076,6373,6209,5987,5962,5747,5775,5829,5772,5492,5383,5320
};
int count = sizeof(dataset) / sizeof(dataset[0]);
//int smallestIndex = count;
int smallestValue = 32767;
float largestValue = 0;
int rangeMax=4095;


void setup() {
  Serial.begin(9600);
  byte clr;
  pinMode(DATAOUT, OUTPUT);
  pinMode(SPICLOCK,OUTPUT);
  pinMode(SLAVESELECT,OUTPUT);
  
  digitalWrite(SLAVESELECT,HIGH); //disable device
  //The SPI control register (SPCR) has 8 bits, each of which control a particular SPI setting.
  
  // SPCR
  // | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |0000000000000000000
  // | SPIE | SPE | DORD | MSTR | CPOL | CPHA | SPR1 | SPR0 |
  
  // SPIE - Enables the SPI interrupt when 1
  // SPE - Enables the SPI when 1
  // DORD - Sends data least Significant Bit First when 1, most Significant Bit first when 0
  // MSTR - Sets the Arduino in master mode when 1, slave mode when 0
  // CPOL - Sets the data clock to be idle when high if set to 1, idle when low if set to 0
  // CPHA - Samples data on the falling edge of the data clock when 1, rising edge when 0'
  // SPR1 and SPR0 - Sets the SPI speed, 00 is fastest (4MHz) 11 is slowest (250KHz)
  
  SPCR = (1<<SPE)|(1<<MSTR)|(1<<CPHA);
  clr=SPSR;
  clr=SPDR;
  delay(20);

  //getMin(dataset[], count );

  for (int i = 0; i < count; i++)
  {
    if (dataset[i] < smallestValue)
    {
      smallestValue = dataset[i];
    }
//    Serial.print(dataset[i]);
//    Serial.println();
  }
  Serial.print("Total Number Of Data Points = ");
  Serial.println(count);
  Serial.print("Smallest value is ");
  Serial.println(smallestValue);

  
  // Iterate through subtracting smallestValue from every data point
  for (int i = 0; i < count; i++)
  {
    dataset[i]=dataset[i]-smallestValue;
  }  
  Serial.println("Smallest value removed from data points");

  // Find the highest value of new dataset and...
  // Multiply by 5 (0-5 volts)
  for (int i = 0; i < count; i++)
  {
    if (dataset[i] > largestValue)
    {
      largestValue = dataset[i];
    }
  }
  Serial.print("Largest value = ");
  Serial.println(largestValue);

  // Divide every data point by the highest value
  for (int i = 0; i < count; i++)
  {
    //Serial.print(dataset[i]);
    //Serial.println(" = ");    
    float pointMarker=dataset[i]/largestValue;
    float pointResult=pointMarker*rangeMax;
    dataset[i]=(int) pointResult;
    
    Serial.print(pointResult);
    Serial.print(" - ");
    Serial.print(dataset[i]);
    Serial.println();
    
  }  
}


void loop() {

  for (int i = 0; i < count; i++)
  {
    SetVoltage(dataset[i]);
    Serial.print(i);
    Serial.print(" : ");
    Serial.print(dataset[i]);
    Serial.println();
    delay(100);
  }  
}

  /////////////////////////////////////////////////////////////////////
  // DAC SPI Interface
  char spi_transfer(volatile char data) {
    SPDR = data; // Start the transmission
    
    // Wait the end of the transmission
    while (!(SPSR & (1<<SPIF))) {
      ;
    }
    return SPDR; // return the received byte
  }
  
  /////////////////////////////////////////////////////////////////////
  // Set the voltage on the 12bit DAC
  byte SetVoltage(short Voltage) {
    Voltage = Voltage | 32768; // Use DAC A
    
    digitalWrite(SLAVESELECT,LOW);
    
    //2 byte opcode -- for some reason we have to do this twice to make it stick with the TLV5618
    spi_transfer(Voltage>>8);
    spi_transfer(Voltage);
    
    spi_transfer(Voltage>>8);
    spi_transfer(Voltage);
    
    digitalWrite(SLAVESELECT,HIGH); //release chip, signal end transfer
  }